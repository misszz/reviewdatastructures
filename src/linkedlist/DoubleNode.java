package linkedlist;

/*双向链表*/
public class DoubleNode {
    //上一个节点
    DoubleNode pre = this;
    //节点内容
    int data;
    //下一个节点
    DoubleNode next = this;

    public DoubleNode() {

    }

    public DoubleNode(int data) {
        this.data = data;
    }

    /*插入节点*/
    public void after(DoubleNode node) {
        //记录当前节点的下一个节点
        DoubleNode nextNode = this.next;
        node.pre = this;
        node.next = nextNode;
        this.next.pre = node;
        this.next = node;
    }

    /*删除当前节点*/
    public void remove() {
        //记录当前节点的上一个节点
        DoubleNode preNode = this.pre;
        //记录当前节点的下一个节点
        DoubleNode nextNode = this.next;
        this.pre.next = nextNode;
        this.next.pre = preNode;
    }
}


/**
 * //记录当前节点的下一个节点
 * DoubleNode nextNode = this.next;
 * node.next = nextNode;
 * node.pre = this;
 * this.next = node;
 * this.next.pre = node;
 **/