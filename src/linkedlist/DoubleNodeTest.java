package linkedlist;

public class DoubleNodeTest {
    public static void main(String[] args) {
        DoubleNode d1 = new DoubleNode(2);
        DoubleNode d2 = new DoubleNode(3);
        DoubleNode d3 = new DoubleNode(5);
        d1.after(d2);
        d1.after(d3);
        d2.remove();
        System.out.println(d1.data);
        System.out.println(d1.next.data);
        System.out.println(d1.next.next.data);


    }
}
