package linkedlist;

public class Node {
    /*节点内容*/
    int data;
    /*下一个节点*/
    Node next;

    public Node() {

    }

    public Node(int data) {
        this.data = data;
    }

    /*追加节点*/
    public Node append(Node next) {
        //记录当前节点
        Node currentNode = this;
        //判断下一个节点是否为空
        while (currentNode.next() != null) {
            //不为空 一直递归寻找下一个节点
            currentNode = currentNode.next();
        }
        //为空的话将要追加的节点添加到当前节点的下一个节点
        currentNode.next = next;
        return next;
    }

    /*获取下一个节点*/
    public Node next() {
        return this.next;
    }

    /*查看当前节点的所有子节点*/
    public void show() {
        //记录当前节点
        Node currentNode = this;
        int index = 1;
        while (true) {
            //先打印当前节点的值
            System.out.println("第" + index + "个节点的值" + currentNode.data);
            if (currentNode.next() == null) {
                break;
            }
            currentNode = currentNode.next();
            index++;
        }

        /*使用这个while会少了最后一种情况
       while (currentNode.next()!=null){
            System.out.println("第"+index+"个节点的值"+currentNode.data);
            currentNode = currentNode.next();
            index++;
        }*/
    }

    /*在一个节点后添加另一个节点*/
    public void after(Node node) {
        //记录当前节点的下一个节点引用
        Node currentNext =this.next();
        this.next = node;
        node.next = currentNext;
    }

    /*删除节点*/
    public void removeNode(){
        this.next = this.next().next();
    }

}
