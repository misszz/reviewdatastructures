package linkedlist;

public class NodeTest {

    public static void main(String[] args) {
        Node node1 = new Node(2);
        node1.append(new Node(3)).append(new Node(5));
        System.out.println(node1.next().next().data);
        node1.show();
        System.out.println("------------------------------------");
        node1.after(new Node(10));
        node1.show();
    }
}
