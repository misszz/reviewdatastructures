package linkedlist;

/**
 * 循环链表 :表中最后一个节点的指针指向头节点
 */
public class LoopNode {
    //节点内容
    int data;
    //下一个节点
    LoopNode next = this;
    public LoopNode(){

    }
    public LoopNode(int data) {
        this.data = data;
    }
    /*获取当前节点的下一个节点*/
    public LoopNode next(){
        return this.next;
    }
    /*添加一个节点*/
    public LoopNode after(LoopNode node){
        LoopNode current =  this.next();
        this.next = node;
        node.next = current;
        return this;
    }

    /*移除当前节点的下一个节点*/
    public void remove(){
        //取出下下一个节点
        LoopNode newNext = this.next().next();
        //将下下一个节点赋值给当前节点的下一个节点
        this.next = newNext;
    }

}
