package linkedlist;

/**
 * 循环链表
 */
public class LoopNodeTest {
    public static void main(String[] args) {
        LoopNode loopNode = new LoopNode(1);
        loopNode.after(new LoopNode(2));
        loopNode.after(new LoopNode(3));

        System.out.println(loopNode.next().data);
        System.out.println(loopNode.next().next().data);
    }
}
