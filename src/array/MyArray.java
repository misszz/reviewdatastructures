package array;

import java.util.Arrays;

public class MyArray {
    //定义一个基本数组
    int[] elements;

    public MyArray() {
        //初始化一个长度为0的数组
        elements = new int[0];
    }

    /**
     * @param num 添加的元素
     *            底层添加一个数组的思想是创建一个新数组把原来的数组遍历赋值给新数组然后再将要添加的数据添加到新数组末尾
     */
    public void add(int num) {
        int[] newElement = new int[elements.length + 1];
        //遍历原数组 将原数组中的值赋值给新数组
        for (int i = 0; i < elements.length; i++) {
            newElement[i] = elements[i];
        }
        //再将要添加的数据赋值给新数组
        newElement[newElement.length - 1] = num;
        //最后再将新数组赋值给原数组
        elements = newElement;
    }

    /**
     * 展示数组中所有的元素
     */
    public void showAllElement() {
        System.out.println(Arrays.toString(elements));
    }

    /**
     * 删除指定下标的元素
     *
     * @param index
     */
    public void delete(int index) {
        if (index < 0 || index >= elements.length) {
            throw new RuntimeException("数组下标越界");
        }
        //创建一个比原数组长度-1的新数组  将原数组中指定索引前面的值赋值给新数组
        int[] newElement = new int[elements.length - 1];
        for (int i = 0; i < elements.length; i++) {
            if (i < index) {
                //不发生改变
                newElement[i] = elements[i];
            } else if (i > index) {
                newElement[i - 1] = elements[i];
            }
        }
        //将新数组赋值给原数组
        elements = newElement;
    }

    /**
     * 向指定索引位置插入某个元素
     *
     * @param index 索引位置
     * @param num   元素
     */
    public void insert(int index, int num) {
        //创建一个新数组
        int[] newElement = new int[elements.length + 1];
        for (int i = 0; i < newElement.length; i++) {
            if (i < index) {
                newElement[i] = elements[i];
            } else if (i == index) {
                newElement[i] = num;
            } else {
                newElement[i] = elements[i - 1];
            }
        }
        //将新数组赋值给原数组
        elements = newElement;
    }


    /**
     * 获取指定索引的值
     *
     * @param index
     * @return
     */
    public int get(int index) {
        return elements[index];
    }

    /**
     * 替换指定位置的元素
     *
     * @param index
     * @param num
     */
    public void replace(int index, int num) {
        elements[index] = num;
    }

    /**
     * 线性查找（遍历所有）
     * 查询元素索引位置，并返回 无返回-1
     *
     * @param num
     * @return
     */
    public int linearSearch(int num) {
        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == num) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 二分法查询
     * 说明：只适用于有序数组
     *
     * @param num
     * @return
     */
    public int binarySearch(int num) {
        if (num < elements[0] || num > elements[elements.length - 1]) {
            throw new RuntimeException("查询的数值不在范围内");
        }
        int begin = 0;
        int end = elements.length - 1;
        int mid = (begin + end) / 2;
        //循环查找
        while (true) {
            /*如何判断数组中有没有该元素 如何解决*/
            if (begin > end) {
                return -1;
            }
            if (num == elements[mid]) {
                return mid;
            } else if (num < elements[mid]) {
                end = mid - 1;
            } else {
                begin = mid + 1;
            }
            mid = (begin + end) / 2;
        }
    }
}
