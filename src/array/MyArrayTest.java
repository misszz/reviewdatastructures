package array;

public class MyArrayTest {
    public static void main(String[] args) {
        //添加元素
        MyArray myArray = new MyArray();
        myArray.add(1);
        myArray.add(2);
        myArray.add(4);
        myArray.add(5);
        myArray.add(6);
        myArray.add(7);
        myArray.showAllElement();
        //删除指定索引的元素
//        myArray.delete(2);
        myArray.showAllElement();
        //向指定索引位置添加元素
//        myArray.insert(1, 999);
        myArray.showAllElement();
        //获取指定索引的值
        System.out.println(myArray.get(1));
        //替换指定位置的元素
//        myArray.replace(1, 222);
        myArray.showAllElement();
//        System.out.println(myArray.linearSearch(222));
        int i = myArray.binarySearch(2);
        System.out.println(i);
    }
}
