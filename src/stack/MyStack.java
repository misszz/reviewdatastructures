package stack;

import java.util.Arrays;

/*用数组来模拟栈存储数据*/
/*栈的特点：先进后出*/
public class MyStack {
    int[] element;

    public MyStack() {
        element = new int[0];
    }

    //入栈
    public void push(int num) {
        int[] newElement = new int[element.length + 1];
        for (int i = 0; i < element.length; i++) {
            newElement[i] = element[i];
        }
        newElement[newElement.length - 1] = num;
        element = newElement;
    }
    //出栈
    public void pop(){
        if (element.length==0){
            throw new RuntimeException("栈顶已为空");
        }
        int[] newElement =new int[element.length-1];
        for (int i = 0; i < newElement.length; i++) {
            newElement[i] = element[i];
        }
        element = newElement;
    }
    //查询栈顶的元素
    public int peek(){
        return element[element.length-1];
    }
    //展示所有元素
    public void show(){
        System.out.println(Arrays.toString(element));
    }
}
