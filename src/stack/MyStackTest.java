package stack;

public class MyStackTest {
    public static void main(String[] args) {
        MyStack myStack = new MyStack();
        myStack.push(1);
        myStack.push(2);
        myStack.push(3);
        myStack.push(4);
        myStack.push(5);
        myStack.pop();
        myStack.pop();
        myStack.show();
        System.out.println(myStack.peek());
    }
}
