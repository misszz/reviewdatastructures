package demo;

/**
 * 斐波那契数列
 */
public class TestFobonaqie {

    public static void main(String[] args) {
        //1 1 2 3 5 8 13 21     第一项和第二项默认为1
        int value = fobonaqi(7);
        System.out.println("value = " + value);
    }

    public static int fobonaqi(int num){
        if (num==1 || num==2){
            return 1;
        }else {
            return fobonaqi(num-1)+fobonaqi(num-2);
        }
    }

}
