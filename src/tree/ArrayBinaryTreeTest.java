package tree;

/**
 * 顺序存储的二叉树：将二叉树存储到数组中
 * 顺序存储的二叉树只考虑完全二叉树
 * 任何一个数组都可以看成一个完全二叉树
 * 第n个元素的左节点：2n+1
 * 第n个元素的右节点：2n+2
 * 第n个元素的父节点：(n-1)/2
 */
public class ArrayBinaryTreeTest {
    public static void main(String[] args) {
        //初始化一个顺序存储的二叉树
        int[] arr={1,2,3,4,5,6,7};
        ArrayBinaryTree tree = new ArrayBinaryTree();
        tree.setData(arr);
        tree.frontShow(0);
    }
}
