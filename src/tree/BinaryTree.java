package tree;

public class BinaryTree {
    //树的根节点
    TreeNode treeNode;

    public TreeNode getTreeNode() {
        return treeNode;
    }

    public void setTreeNode(TreeNode treeNode) {
        this.treeNode = treeNode;
    }

    public void frontShow() {
        treeNode.frontShow();
    }

    public void midShow() {
        treeNode.midShow();
    }

    public void afterShow() {
        treeNode.afterShow();
    }

    public TreeNode frontSearch(int i) {
        return treeNode.frontSearch(i);
    }
}
