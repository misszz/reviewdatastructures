package tree;

public class BinaryTreeTest {
    public static void main(String[] args) {
        BinaryTree binaryTree = new BinaryTree();
        TreeNode t1 = new TreeNode(1);
        TreeNode t2 = new TreeNode(2);
        TreeNode t3 = new TreeNode(3);
        TreeNode t4 = new TreeNode(4);
        TreeNode t5 = new TreeNode(5);
        TreeNode t6 = new TreeNode(6);

        //设置当前节点为根节点
        binaryTree.setTreeNode(t1);;
        t1.setLeftNode(t2);
        t1.setRightNode(t3);
        t2.setLeftNode(t4);
        t2.setRightNode(t5);
        t3.setLeftNode(t6);
        //获取根节点
//        System.out.println(binaryTree.getTreeNode().data);
//        System.out.println(binaryTree.getTreeNode().getLeftNode().data);
//        System.out.println(binaryTree.getTreeNode().getRightNode().data);
        //前序遍历二叉树
//        binaryTree.frontShow();
        //中序遍历二叉树
//        binaryTree.midShow();
        //后序遍历二叉树
//        binaryTree.afterShow();
        //前序查找
        TreeNode treeNode = binaryTree.frontSearch(7);
        System.out.println(treeNode);
    }
}
