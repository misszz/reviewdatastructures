package tree;

/**
 * 顺序存储的二叉树：将二叉树存储到数组中
 * 顺序存储的二叉树只考虑完全二叉树
 * 第n个元素的左节点：2n+1
 * 第n个元素的右节点：2n+2
 * 第n个元素的父节点：(n-1)/2
 */
public class ArrayBinaryTree {
    int[] data;

    public ArrayBinaryTree() {
    }

    public int[] getData() {
        return data;
    }

    public void setData(int[] data) {
        this.data = data;
    }


    public void frontShow(int index) {
        if (data == null || data.length == 0) {
            return;
        }
        System.out.println(data[index]);
        if (2 * index + 1 < data.length) {
            frontShow(2 * index + 1);
        }
        if (2 * index + 2 < data.length) {
            frontShow(2 * index + 2);
        }
    }
}
