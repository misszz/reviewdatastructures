package tree;

public class TreeNode {
    //左节点
    TreeNode leftNode;
    //当前节点的值
    int data;
    //右节点
    TreeNode rightNode;

    public TreeNode() {
    }

    public TreeNode(int data) {
        this.data = data;
    }

    public void setLeftNode(TreeNode leftNode) {
        this.leftNode = leftNode;
    }

    public void setRightNode(TreeNode rightNode) {
        this.rightNode = rightNode;
    }

    public TreeNode getLeftNode() {
        return leftNode;
    }

    public TreeNode getRightNode() {
        return rightNode;
    }

    //前序遍历：根左右
    public void frontShow() {
        //打印当前节点的值
        System.out.println(data);
        if (leftNode != null) {
            leftNode.frontShow();
        }
        if (rightNode != null) {
            rightNode.frontShow();
        }
    }

    //中序遍历：左根右
    public void midShow() {
        if (leftNode!=null){
            leftNode.midShow();
        }
        System.out.println(data);
        if (rightNode!=null){
            rightNode.midShow();
        }
    }
    //后序遍历：左右根
    public void afterShow() {
        if (leftNode!=null){
            leftNode.afterShow();
        }
        if (rightNode!=null){
            rightNode.afterShow();
        }
        System.out.println(data);
    }
    //前序查找
    public TreeNode frontSearch(int i) {
        //如果当前节点的值等于查找的值  返回当前节点
        if (data ==i){
            return this;
        }else {
            TreeNode res = null;
            //先查找左节点
            if (leftNode!=null){
                res = leftNode.frontSearch(i);
                if (res!=null){
                    return res;
                }
            }
            if (rightNode!=null){
                res = rightNode.frontSearch(i);
                if (res!=null){
                    return res;
                }
            }
            return null;
        }
    }


}
