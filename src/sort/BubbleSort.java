package sort;

import java.util.Arrays;

/**
 * 冒泡排序
 * 方向：从小到大
 */
public class BubbleSort {
    public static void main(String[] args) {
        int[] element = {1, 2, 4, 8, 3, 5};
        //最外层比较的是轮数
        for (int i = 0; i < element.length - 1; i++) {
            //每轮需要比较的次数
            for (int j = 0; j < element.length - 1 - i; j++) {
                if (element[j] > element[j + 1]) {
                    int temp = element[j];
                    element[j] = element[j + 1];
                    element[j + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(element));
    }
}
