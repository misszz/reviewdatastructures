package queue;

import java.util.Arrays;

/*队列特点：先进先出*/
public class MyQueue {
    private int[] element;

    public MyQueue() {
        element = new int[0];
    }

    /*入列*/
    public void add(int num) {
        int[] newElement = new int[element.length + 1];
        for (int i = 0; i < element.length; i++) {
            newElement[i] = element[i];
        }
        newElement[newElement.length - 1] = num;
        //将新数组赋值给原数组
        element = newElement;
    }

    /*出列*/
    public void outQueue() {
        if (element.length==0){
            throw new RuntimeException("队列已空");
        }
        int[] newElement = new int[element.length - 1];
        for (int i = 0; i < newElement.length; i++) {
            newElement[i] = element[i + 1];
        }
        element = newElement;
    }

    /*查看队列中所有元素*/
    public void show() {
        System.out.println(Arrays.toString(element));
    }
    /*判断队列是否为空*/
    public boolean isEmpty(){
        return element.length==0?true:false;
    }
}
